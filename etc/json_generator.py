from PIL import Image
import glob, os, sys
import json
import datetime
import random

class ImData:
	def __init__(self, id, name, src):
		self.id = id
		self.name = name
		self.src = src
		self.err = err
		self.avgcolor = avgcolor


def get_img_data(filename):
	i = Image.open(filename)
	h = i.histogram()

	# split into red, green, blue
	r = h[0:256]
	g = h[256:256*2]
	b = h[256*2: 256*3]

	# perform the weighted average of each channel:
	# the *index* is the channel value, and the *value* is its weight
	sumwr, sumr = sum(i*w for i, w in enumerate(r)), sum(r)
	sumwg, sumg = sum(i*w for i, w in enumerate(g)), sum(g)
	sumwb, sumb = sum(i*w for i, w in enumerate(b)), sum(b)
	return (
		-1 if sumr == 0 else sumwr/sumr,
		-1 if sumr == 0 else sumwg/sumg,
		-1 if sumr == 0 else sumwb/sumb
	), i.size


def get_random_date():
	'''
	<script>
	var d = new Date("October 13, 2014 11:13:00");
	document.getElementById("demo").innerHTML = d;
	</script> 
	'''
	year = random.randint(2000, 2014)
	month = random.randint(1, 12)
	day = random.randint(1, 28)
	hour = random.randint(1, 23)
	min = random.randint(1, 59)
	sec = random.randint(1, 59)
	dt = datetime.datetime(year, month, day, hour, min, sec)
	return dt.strftime("%B %d, %Y %H:%M:%S")

sizes = ['a', 'b', 'c', 'd']
sz1 = 820000
sz2 = 4000000
sz3 = 9000000

def get_img_size(sz):
	pxls = sz[0] * sz[1]
	if pxls <= sz1:
		return 'S'
	elif pxls > sz1 and pxls <= sz2:
		return 'M'
	elif pxls > sz2 and pxls <= sz3:
		return 'L'
	elif pxls > sz3:
		return 'XL'


def get_random_lat_long():
	lat = round((1 + random.random() * 88), 3)
	long = round((1 + random.random() * 178), 3)
	return lat, long


walk_dir = 'D:\\splashbase\\imgbase\\'

if __name__ == '__main__':
	kount = 1
	jsondata = ''
	jsondata += '{'
	jsondata += '\"images\":['
	for root, subdirs, files in os.walk(walk_dir):
		for filename in files:
			if not filename.endswith('.jpg'):
				break
			file_path = os.path.join(root, filename)
			err = ''
			try:
				avgcolor, sz = get_img_data(file_path)
			except Exception as e:
				avgcolor = -1, -1, -1
				err = 'Error'
			file_path = file_path.replace(walk_dir, "")
			imdata = ImData(kount, filename, file_path)
			imdata.err = err
			imdata.avgcolor = avgcolor
			imdata.time = get_random_date()
			imdata.latlong = get_random_lat_long()
			imdata.size = get_img_size(sz)
			jsondata += ',' if kount > 1 else ''
			jsondata += json.dumps(imdata.__dict__)
			kount += 1
	jsondata += ']'
	jsondata += '}'
	print jsondata