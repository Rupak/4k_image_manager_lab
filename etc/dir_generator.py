from PIL import Image
import glob, os, sys
import json
import datetime
import random

class DirData:
	def __init__(self, loc, childs, file, is_root):
		self.loc = loc
		self.childs = childs
		self.file = file
		self.is_root = is_root

walk_dir = 'D:\\splashbase\\imgbase'

def get_any_file(dirpath):
	f = []
	for root, subdirs, files in os.walk(dirpath):
		for file in files:
			f.append(file)
	if len(f) < 1:
		return ''
	else:
		import random
		return f[random.randint(0, len(f)-1)]

if __name__ == '__main__':
	kount = 1
	jsondata = ''
	jsondata += '{'
	jsondata += '\"dirs\":['
	for root, subdirs, files in os.walk(walk_dir):
		ddata = DirData(root, subdirs, get_any_file(root), root == walk_dir)
		ddata.id = kount
		jsondata += ',' if kount > 1 else ''
		jsondata += json.dumps(ddata.__dict__)
		kount += 1
	jsondata += ']'
	jsondata += '}'
	print jsondata
