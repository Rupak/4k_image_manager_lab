init();

//$(function() {
//      $( "#selectableGrid" ).selectable();
//});

$("#searchModalOptionsBtn").click(function() {
	$("#searchModalOptions").toggle();
});

$('#colorpaletteBrowse').change(function() {
	var color = $('#colorpaletteBrowse .color').val();
	var cint = hexToRgb(color);
	showSomeImage(-1, 100, matchColorRange, [cint.r, cint.g, cint.b]);
});

$(function() {
	$("#datepicker_b_start").datepicker();
	$("#datepicker_b_end").datepicker();
	$("#datepicker_s_start").datepicker();
	$("#datepicker_s_end").datepicker();
});

$('#searchModal').on('shown.bs.modal', function () {
    $('#searchBox').focus();
})

$(document).on( 'shown.bs.tab', 'a[data-toggle="tab"]', function (e) {
	google.maps.event.trigger(map, "resize");
	google.maps.event.trigger(mapBrowse, "resize");
});

function getImageLiStr(imdata) {
	var str = '';
	var src = imgBaseDir + imdata.src;
	src = src.replace("\\", "/");
	str += '<li class="block">';
	str += '<a href="' + src + '" data-lightbox="imgset" data-title="">';
	src = thumbBaseDir + imdata.name;
	src = src.replace("\\", "/");
	str += '<img class="thumb" src="' + src + '" />';
	str += '</a>';
	str += '</li>';
	return str;
}

function loadImgData(url) {
	$.getJSON(url, function(json) {
		$('.img_container ul').empty();
		for(var i = 0; i < json.images.length; i++) {
			$(".img_container ul").append(getImageLiStr(json.images[i]));
		}
    })
}

function showSomeImage(start, kount, filter, param) {
	var nimgs = allImages.length;
	if (start < 0) {
		start = 0;
	}
	if (kount < 0 || kount >= maxImagePerPage) {
		kount = maxImagePerPage;
	}
	$('.img_container ul').empty();
	k = 0;
	if (filter == matchColorRange) {
		for(var i = start; i < nimgs && k < kount; i++) {
			if (matchColorRange(allImages[i], param)) {
				$(".img_container ul").append(getImageLiStr(allImages[i]));
				k ++;
			}
		}
	} else {
		for(var i = start; i < nimgs && k < kount; i++) {
			$(".img_container ul").append(getImageLiStr(allImages[i]));
			k ++;
		}
	}
}

function loadAllImgData() {
	$.getJSON("etc/tdata.json", function(json) {
		allImages = json.images;
		//showSomeImage(-1, 100, matchColorRange, [176, 41, 40]);
		showSomeImage(-1, 100);
    })
}

function hexToRgb(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}

function inColorRange(c, refc) {
	l = Math.max(0, refc - 50);
	h = Math.min(255, refc + 50);
	return c >= l && c <= h;
}

function matchColorRange(imdata, refrange) {
	r = imdata.avgcolor[0];
	g = imdata.avgcolor[1];
	b = imdata.avgcolor[2];
	refr = refrange[0];
	refg = refrange[1];
	refb = refrange[2];
	return inColorRange(r, refr) && inColorRange(g, refg) && inColorRange(b, refb);
}

function browseTag(obj) {
	var tag = obj.getAttribute('data-tag');
	loadRandomImgData();
}

function browseSize(obj) {
	var tag = obj.getAttribute('data-size');
	loadRandomImgData();
}

function browseColor(obj) {
	var tag = obj.getAttribute('data-color');
	loadRandomImgData();
}

function init() {
	allImages = {};
	maxImagePerPage = 100;
	curImgIndex = 0;
	imgBaseDir = 'file:///D:/splashbase/imgbase/';
	thumbBaseDir = 'file:///D:/splashbase/thumbs/wh160/';

	loadAllImgData();
	$("#searchModalOptions").hide();
	$("#dirUpButton").hide();
	loaddir("etc/dir.json", true);
}

function loadRandomImgData() {
	showSomeImage(curImgIndex, curImgIndex + maxImagePerPage);
	curImgIndex = curImgIndex + maxImagePerPage;
	if (curImgIndex + maxImagePerPage > allImages.length) {
		curImgIndex = 1;
	}
}

Mousetrap.bind('s', function() {
	$('#browseModal').modal('hide');
	$('#searchModal').modal('toggle');
});

Mousetrap.bind('b', function() {
	$('#searchModal').modal('hide');
	$('#browseModal').modal('toggle');
});

Mousetrap.bind('n', function() {
	if (curImgIndex + maxImagePerPage < allImages.length - 1) {
		curImgIndex = curImgIndex + maxImagePerPage;
		showSomeImage(curImgIndex, curImgIndex + maxImagePerPage);
	}
});

Mousetrap.bind('p', function() {
	if (curImgIndex - maxImagePerPage >= 0) {
		curImgIndex = curImgIndex - maxImagePerPage;
		showSomeImage(curImgIndex, curImgIndex + maxImagePerPage);
	}
});

Mousetrap.bind('d', function() {
	var currentDate = $( "#datepicker" ).datepicker( "getDate" );
	alert('Debug:' + currentDate);
});

var dirInfo;
var curdirnode;

function dirUp() {
	showExplorer(getParent(curdirnode));
}

function getRootDir() {
	for (var i=0;i<dirInfo.dirs.length; i++) {
		if (dirInfo.dirs[i].is_root) {
			return dirInfo.dirs[i];
		}
	}
}

function getParent(node) {
	var arr = node.loc.split('\\');
	var name = arr[arr.length - 1];
	for (var i=0;i<dirInfo.dirs.length; i++) {
		for (var j=0;j<dirInfo.dirs[i].childs.length; j++) {
			if (dirInfo.dirs[i].childs[j] == name) {
				return dirInfo.dirs[i];
			}
		}
	}
}

function getNodeById(id) {
	for (var i=0;i<dirInfo.dirs.length; i++) {
		if (dirInfo.dirs[i].id == id) {
			return dirInfo.dirs[i];
		}
	}
}

function getNodeId(loc) {
	for (var i=0;i<dirInfo.dirs.length; i++) {
		if (dirInfo.dirs[i].loc == loc) {
			return dirInfo.dirs[i].id;
		}
	}
}

function getFolderItems(node, childname) {
	cnodeloc = node.loc + '\\' + childname;
	cnodeid = getNodeId(cnodeloc);
	cnode = getNodeById(cnodeid);
	var str = '';
	str += '<li class="block">';
	// show explorer with child
	str += '<a href="#" onclick="showExplorer(getNodeById(' + cnodeid + '));">';
	str += '<img class="folder" src="' + thumbBaseDir + cnode.file + '" />';
	str += '<br><span>'+ childname +'</span>';
	str += '</a>';
	str += '</li>';
	return str;
}

function showExplorer(node) {
	curdirnode = node;
	$('.dir_container ul').empty();
	if (!node.is_root) {
		$("#dirUpButton").show();
	} else {
		$("#dirUpButton").hide();
	}
	for(var i = 0; i < node.childs.length; i++) {
		var str = getFolderItems(node, node.childs[i]);
		$(".dir_container ul").append(str);
	}
	if (init == false) {
		loadRandomImgData();
	}
}

function loaddir(url, init){
	$.getJSON(url, function(json) {
		dirInfo = json;
		showExplorer(getRootDir());
    })
}

var map, mapBrowse;
var oldBounds;

function addRandomMapMarkers() {
	var markers = [];
	var k=0;
	for (var lat=-85;lat <= 85; lat += 5) {
		for (var lng=-175;lng <= 175; lng += 5) {
			var llat = lat + Math.random() * 4;
			var llng = lng + Math.random() * 4;
			markers[k++] = new google.maps.Marker({
				position: new google.maps.LatLng(llat, llng)
			});
		}
	}
	for (var i=0;i<k;i++) {
		markers[i].setMap(mapBrowse);
		google.maps.event.addListener(markers[i], 'click', function() {
			alert('hi');
		});
	}
}

function initialize() {
	var mapOptions = {
		zoom: 8,
		center: new google.maps.LatLng(48.7667, 9.1833)
	};
	map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
	mapBrowse = new google.maps.Map(document.getElementById('map-canvas-browse'), mapOptions);

	google.maps.event.addListener(mapBrowse, 'idle', function() {
		var bounds = mapBrowse.getBounds();
		if (!bounds.equals(oldBounds) && mapBrowse.getDiv().offsetWidth > 0) {
			boundingBoxChanged();
		}
		oldBounds = bounds;
	});
	addRandomMapMarkers();
}

function boundingBoxChanged() {
	loadRandomImgData();
}

google.maps.event.addDomListener(window, 'load', initialize);